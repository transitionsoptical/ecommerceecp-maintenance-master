﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using eCommerceEcpMaintenance.Extensions;
using eCommerceEcpMaintenance.Models;
using eCommerceEcpMaintenance.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace eCommerceEcpMaintenance.Controllers
{
    public class ECommerceEcpController : Controller
    {
        private readonly ApiSettings _apiSettings;

        public ECommerceEcpController(IConfiguration configuration)
        {
            _apiSettings = new ApiSettings(configuration);
        }

        public ECommerceEcpViewModel GetById(int id)
        {
            var ecp = new ECommerceEcpViewModel();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_apiSettings.EndPoint);
                //HTTP GET
                var responseTask = client.GetAsync(_apiSettings.FindByIdRequest + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsJsonAsync<ECommerceEcpViewModel>();
                    readTask.Wait();

                    ecp = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }

            return ecp;
        }

        public async Task<IActionResult> SearchByCountry(string searchString)
        {
            IEnumerable<ECommerceEcpViewModel> ecps = new List<ECommerceEcpViewModel>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_apiSettings.EndPoint);
                //HTTP GET
                var responseTask = client.GetAsync(searchString);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsJsonAsync<IList<ECommerceEcpViewModel>>();
                    readTask.Wait();

                    ecps = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    ecps = Enumerable.Empty<ECommerceEcpViewModel>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View("Index", ecps);
        }

        public IActionResult Index()
        {
            IEnumerable<ECommerceEcpViewModel> ecps = new List<ECommerceEcpViewModel>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_apiSettings.EndPoint);
                //HTTP GET
                var responseTask = client.GetAsync(_apiSettings.GetAllRequest);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsJsonAsync<IList<ECommerceEcpViewModel>>();
                    readTask.Wait();

                    ecps = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    ecps = Enumerable.Empty<ECommerceEcpViewModel>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(ecps);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ECommerceEcpViewModel viewModel)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_apiSettings.EndPoint);

                var postTask = client.PostAsJsonAsync<ECommerceEcpViewModel>(_apiSettings.PostRequest, viewModel);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(viewModel);
        }

        public ActionResult Edit(int id)
        {
            ECommerceEcpViewModel viewModel = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_apiSettings.EndPoint);
                //HTTP GET
                var responseTask = client.GetAsync(_apiSettings.FindByIdRequest + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsJsonAsync<ECommerceEcpViewModel>();
                    readTask.Wait();

                    viewModel = readTask.Result;
                }
            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(ECommerceEcpViewModel viewModel)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_apiSettings.EndPoint);

                //HTTP POST
                var putTask = client.PutAsJsonAsync<ECommerceEcpViewModel>(_apiSettings.PutRequest + viewModel.Id.ToString(), viewModel);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }
            return View(viewModel);
        }

        public async Task<IActionResult> Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ViewData["ErrorMessage"] =
                    "Delete failed. Try again, and if the problem persists " +
                    "see your system administrator.";
            }

            var recordToDelete = GetById(id.Value);

            return View(recordToDelete);
        }

        public ActionResult DeleteConfirmed(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_apiSettings.EndPoint);

                var deleteTask = client.DeleteAsync(_apiSettings.DeleteRequest + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }
    }
}
