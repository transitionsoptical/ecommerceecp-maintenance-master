﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace eCommerceEcpMaintenance.Models
{
    public class ECommerceEcpViewModel
    {
        public ECommerceEcpViewModel() { }

        public int Id { get; set; }

        [DisplayName("ECP")]
        public string Ecp { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Country Code")]
        public string CountryCode { get; set; }

        [DisplayName("Transitions ID")]
        public string TransitionsId { get; set; }

        [DisplayName("Online Only")]
        public bool IsOnlineOnly { get; set; }

        [DisplayName("Online And In Store")]
        public bool IsOnlineAndInStore { get; set; }

        [DisplayName("Site URL")]
        public string WebsiteUrl { get; set; }

        [DisplayName("Platform")]
        public string Platform { get; set; }

        [DisplayName("Countries Available")]
        public string CountriesAvailable { get; set; }

        [DisplayName("Image Path")]
        public string ImagePath { get; set; }
    }
}
