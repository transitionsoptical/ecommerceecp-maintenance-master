﻿
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerceEcpMaintenance.Utilities
{
    public class ApiSettings
    {
        public ApiSettings(IConfiguration configuration)
        {
            EndPoint = configuration.GetSection("ApiSettings").GetSection("EndPoint").Value;
            PostRequest = configuration.GetSection("ApiSettings").GetSection("PostRequest").Value;
            PutRequest = configuration.GetSection("ApiSettings").GetSection("PutRequest").Value;
            DeleteRequest = configuration.GetSection("ApiSettings").GetSection("DeleteRequest").Value;
            FindByIdRequest = configuration.GetSection("ApiSettings").GetSection("FindByIdRequest").Value;
            GetAllRequest = configuration.GetSection("ApiSettings").GetSection("GetAllRequest").Value;
        }

        public string EndPoint { get; protected set; }

        public string PostRequest { get; protected set; }

        public string PutRequest { get; protected set; }

        public string DeleteRequest { get; protected set; }

        public string FindByIdRequest { get; protected set; }

        public string GetAllRequest { get; protected set; }
    }
}
